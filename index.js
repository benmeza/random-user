const express = require('express');

// route for users
const users = require('./routes/users');

const app = express();
app.use(express.json());

app.use('/users', users);

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => console.log(`Server running on port ${PORT}`));
