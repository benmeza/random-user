const express = require('express');
const axios = require('axios');
const router = express.Router();

// Used to store users
let tempUsers = [];

/**
 * Makes api call to retrieve user data
 */
const getUser = async () => {
  try {
    const url = 'https://randomuser.me/api';
    const response = await axios.get(url);
    const user = response.data.results[0];
    const rtn = {
      gender: user.gender,
      firstname: user.name.first,
      city: user.location.city,
      email: user.email,
      cell: user.cell
    }
    return rtn;
  } catch (err) {
    console.log(err);
  }
};

/**
 * Creates multiple users based on count
 * @param {number} count Number of users to create
 */
const getUsers = async (count) => {
  return Promise.all([...Array(count)].map(() => {
    return getUser();
  }))
}

/**
 * Validates the user object for all propertiesd
 * @param {object} user user object to validate
 */
const validateUser = (user) => {
  const props = ['gender', 'firstname', 'city', 'email', 'cell'];
  return props.reduce((missing, prop) => {
    if (!user.hasOwnProperty(prop)) {
      missing.push(prop);
    };
    return missing;
  }, []);
}

/**
 * Get 10 users and store to memory
 * 
 * To get current users in memory without
 *  creating 10 new ones use:
 *  "?getcurrent=true"
 */
router.get('/', (req, res) => {
  if (req.query.getcurrent === 'true') {
    res.status(200).json(tempUsers);
  } else{
    getUsers(10).then(data => {
      tempUsers = tempUsers.concat(data);
      res.status(200).json(tempUsers);
    });
  }
});

/**
 * Create User
 */
router.post('/', (req, res) => {
  const missing = validateUser(req.body);
  if (missing.length === 0) {
    const user = {
      gender: req.body.gender,
      firstname: req.body.firstname,
      city: req.body.city,
      email: req.body.email,
      cell: req.body.cell
    };
    tempUsers.push(user);
    res.status(201).json({ message: 'User successfully created!' });
  } else {
    res.status(400).json({ message: `Object Validation error: [${missing.toString()}]`})
  }
  
});

/**
 * filter users by first name
 */
router.get('/firstname/:firstname', (req, res) => {
  const filter = req.params.firstname;
  const filtered = tempUsers.filter((user) => {
    return user.firstname === filter;
  });

  if (filtered.length === 0) {
    res.status(404).json( { message: 'User not found!' } );
  } else {
    res.status(200).json(filtered);
  }
});

module.exports = router;
